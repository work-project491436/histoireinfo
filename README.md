# Projet École Sup de Vinci Paris

Bienvenue sur le dépôt du projet réalisé par la team **DEV-B** de l'École Sup de Vinci Paris.

## Présentation

Ce site a été conçu et développé dans le cadre de notre cursus à l'École Sup de Vinci Paris. Il représente les compétences, l'engagement et la passion de notre équipe pour la technologie et le développement web.

## Technologies utilisées

- HTML/CSS
- JavaScript

## Comment exécuter le projet ?
https://histoireinfo-work-project491436-0b90764d713e80b2635120b673f0bf1.gitlab.io
1. Clonez le dépôt sur votre machine locale.
2. Démarrez avec Liveserver ou directement sur le navigateur.



Merci de votre intérêt pour notre projet. Nous espérons que vous l'apprécierez autant que nous avons apprécié le développer !
